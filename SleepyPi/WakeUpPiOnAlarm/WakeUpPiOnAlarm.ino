// Set the RTC alarm pin to wake up the Arduino everyday
// and then power up the Raspberry Pi
// RPi will perform some action and will power down ifself.
// A day after, Arduino will repeat procedure above

#include "SleepyPi.h"
#include <Time.h>
#include <LowPower.h>
#include <DS1374RTC.h>
#include <Wire.h>

const int LED_PIN = 13;

int counter = 3;

float battery_threshold = 11.4;
float voltageValue = 0.0;

void alarm_isr()
{
    // Just a handler for the alarm interrupt.
    // You could do something here

}

void setup()
{

  // Configure "Standard" LED pin
  pinMode(LED_PIN, OUTPUT);		
  digitalWrite(LED_PIN,LOW);		// Switch off LED

  SleepyPi.enablePiPower(false);  

  // initialize serial communication: In Arduino IDE use "Serial Monitor"
  Serial.begin(9600);
  counter = 3;

}

void loop() 
{
      for (int i=0; i < 3; i++){
        digitalWrite(LED_PIN,HIGH);		// Switch on LED
        delay(1000);
        digitalWrite(LED_PIN,LOW);		// Switch off LED
        delay(1000);
      }

  // Allow wake up alarm to trigger interrupt on falling edge.
      attachInterrupt(0, alarm_isr, FALLING);		// Alarm pin
      SleepyPi.enableWakeupAlarm();
      SleepyPi.setAlarm(counter);              // initial counter is 3 sec

      // Enter power down state with ADC and BOD module disabled.
      // Wake up when wake up pin is low.
      SleepyPi.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF);
      // Disable external pin interrupt on wake up pin.
      detachInterrupt(0);

      /* *********************************

      Sleep .... zzz
      Wake up when interrupt is occurred by RTC
      */
      digitalWrite(LED_PIN,HIGH);		// Switch on LED
	  
	  int sensorValue = analogRead(A1);
	  voltageValue = sensorValue * (3.3 / 1023.0) * 5.0;
	  Serial.println(voltageValue);

	  if (voltageValue < battery_threshold){
		  Serial.println("Move to long-sleep mode...");
		  counter = 3600; 			// Wake up after an hour
	  }
	  else{
		  SleepyPi.enablePiPower(true);
		  sync_with_RPi();
		  delay(2000);
		  SleepyPi.enablePiPower(false);
	  }
}

void sync_with_RPi(){
      // Send greeting, important...

      while (!Serial) {
        ; // wait for serial port to connect. Needed for Leonardo only
      }

      // Sync with RPi
      int val = -1;
      while (val != 50){
        val = Serial.read();
      }
      Serial.println(val);
      while (val != 100){
        val = Serial.read();
      }
      Serial.println(val);
      while (val != 50){
        val = Serial.read();
      }
      Serial.println(val);
      while (val != 75){
        val = Serial.read();
      }
      Serial.println(val);

      // receive hours
      val = -1;
      while (val == -1){
        val = Serial.read();
      }
      Serial.println(val);  // reply received value : important
      counter = val * 60;
      val = -1;

      // receive minutes
      while (val == -1){
        val = Serial.read();
      }
      Serial.println(val);  // reply received value : important
      counter += val;

	  Serial.println(voltageValue);	// Send voltage value to store it to cloud.

      counter *= 60;        // convert minute to second

}
