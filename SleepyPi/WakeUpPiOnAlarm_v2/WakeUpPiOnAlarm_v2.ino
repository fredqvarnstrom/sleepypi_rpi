// Set the RTC alarm pin to wake up the Arduino everyday
// and then power up the Raspberry Pi
// RPi will perform some action and will power down ifself.
// A day after, Arduino will repeat procedure above

#include "SleepyPi2.h"
#include <Time.h>
#include <LowPower.h>
#include <PCF8523.h>
#include <Wire.h>

const char *monthName[12] = {
  "Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
};

const int LED_PIN = 13;

eTIMER_TIMEBASE  PeriodicTimer_Timebase     = eTB_MINUTE;   // e.g. Timebase set to seconds
int counter = 1;

tmElements_t tm;

float battery_threshold = 11.4;
float voltageValue = 0.0;

void alarm_isr()
{
    // Just a handler for the alarm interrupt.
    // You could do something here

}

void setup()
{

	// Configure "Standard" LED pin
	pinMode(LED_PIN, OUTPUT);		
	digitalWrite(LED_PIN,LOW);		// Switch off LED
	
	delay(1000);
	
	// initialize serial communication: In Arduino IDE use "Serial Monitor"
	Serial.begin(9600);
	printTimeNow();
	Serial.println("Starting, but I'm going to go to sleep for a while...");
	counter = 1;
	
	SleepyPi.rtcInit(true);
	
	// Default the clock to the time this was compiled.
	// Comment out if the clock is set by other means
	// ...get the date and time the compiler was run
	if (getDate(__DATE__) && getTime(__TIME__)) {
		// and configure the RTC with this info
		SleepyPi.setTime(DateTime(F(__DATE__), F(__TIME__)));
	} 
}

void loop() 
{
	for (int i=0; i < 3; i++){
		digitalWrite(LED_PIN,HIGH);		// Switch on LED
		delay(1000);
		digitalWrite(LED_PIN,LOW);		// Switch off LED
		delay(1000);
	}

	SleepyPi.rtcClearInterrupts();
	
    // Allow wake up alarm to trigger interrupt on falling edge.
    attachInterrupt(0, alarm_isr, FALLING);		// Alarm pin
      
	// Set the Periodic Timer
    SleepyPi.setTimer1(PeriodicTimer_Timebase, counter);
	
	delay(500);
	
	// Enter power down state with ADC and BOD module disabled.
    // Wake up when wake up pin is low.
    SleepyPi.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF); 

    // Disable external pin interrupt on wake up pin.
    detachInterrupt(0);
    
    SleepyPi.ackTimer1();
    /* *********************************

    Sleep .... zzz
    Wake up when interrupt is occurred by RTC
    */
    digitalWrite(LED_PIN,HIGH);		// Switch on LED
	  
	int sensorValue = analogRead(A6);
	voltageValue = 12.0 * sensorValue / 197.46;
	
//    if (voltageValue < battery_threshold){
//	    Serial.println("Move to long-sleep mode...");
//		counter = 60; 			// Wake up after an hour
//	}
//	else{
		SleepyPi.enablePiPower(true);
		printTimeNow();
		sync_with_RPi();
		delay(2000);
		SleepyPi.enablePiPower(false);
//	}
}

void sync_with_RPi(){
      // Send greeting, important...

      while (!Serial) {
        ; // wait for serial port to connect. Needed for Leonardo only
      }

      // Sync with RPi
      int val = -1;
      while (val != 50){
        val = Serial.read();
      }
      Serial.println(val);
      while (val != 100){
        val = Serial.read();
      }
      Serial.println(val);
      while (val != 50){
        val = Serial.read();
      }
      Serial.println(val);
      while (val != 75){
        val = Serial.read();
      }
      Serial.println(val);

      // receive hours
      val = -1;
      while (val == -1){
        val = Serial.read();
      }
      Serial.println(val);  // reply received value : important
      counter = val * 60;
      val = -1;

      // receive minutes
      while (val == -1){
        val = Serial.read();
      }
      Serial.println(val);  // reply received value : important
      counter += val;
      
      Serial.println(counter);
		
	  Serial.println(voltageValue);	// Send voltage value to store it to cloud.
}
void printTimeNow()
{
    // Read the time
    DateTime now = SleepyPi.readTime();
    
    // Print out the time
    Serial.print("Ok, Time = ");
    print2digits(now.hour());
    Serial.write(':');
    print2digits(now.minute());
    Serial.write(':');
    print2digits(now.second());
    Serial.print(", Date (D/M/Y) = ");
    Serial.print(now.day());
    Serial.write('/');
    Serial.print(now.month()); 
    Serial.write('/');
    Serial.print(now.year(), DEC);
    Serial.println();

    return;
}
bool getTime(const char *str)
{
  int Hour, Min, Sec;

  if (sscanf(str, "%d:%d:%d", &Hour, &Min, &Sec) != 3) return false;
  tm.Hour = Hour;
  tm.Minute = Min;
  tm.Second = Sec;
  return true;
}

bool getDate(const char *str)
{
  char Month[12];
  int Day, Year;
  uint8_t monthIndex;

  if (sscanf(str, "%s %d %d", Month, &Day, &Year) != 3) return false;
  for (monthIndex = 0; monthIndex < 12; monthIndex++) {
    if (strcmp(Month, monthName[monthIndex]) == 0) break;
  }
  if (monthIndex >= 12) return false;
  tm.Day = Day;
  tm.Month = monthIndex + 1;
  tm.Year = CalendarYrToTm(Year);
  return true;
}

void print2digits(int number) {
  if (number >= 0 && number < 10) {
    Serial.write('0');
  }
  Serial.print(number);
}

