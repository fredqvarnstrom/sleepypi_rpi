
int cnt = 0;

void setup()
{
  // start serial port at 9600 bps:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for  port to connect. Needed for Leonardo only
  }
}

void loop()
{
  // if we get a valid byte, read analog ins:
  if (Serial.available() > 0) {
    Serial.println(cnt);
    if(cnt < 100){
      cnt ++;  
    }
    else{
      cnt = 0;
    }
  }
  delay(1000);
}

